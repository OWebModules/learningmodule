﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Converters
{
    public static class IntToCategoryConverter
    {
        public static clsOntologyItem Convert(int categoryId)
        {
            switch (categoryId)
            {
                case 0:
                    return Config.LocalData.Object_Wei__ich_nicht;
                    
                case 1:
                    return Config.LocalData.Object_Wei__ich;
                    
                case 2:
                    return Config.LocalData.Object_Sitzt;
                    
                case 3:
                    return Config.LocalData.Object_Offizielle_Antwort;
                    
                default:

                    return null;

            }
        }

        public static int Convert(clsOntologyItem categoryItem)
        {
            if (categoryItem == null)
            {
                return -1;
            }
            if (categoryItem.GUID == Config.LocalData.Object_Wei__ich_nicht.GUID)
            {
                return 0;
            }
            if (categoryItem.GUID == Config.LocalData.Object_Wei__ich.GUID)
            {
                return 1;
            }
            if (categoryItem.GUID == Config.LocalData.Object_Sitzt.GUID)
            {
                return 2;
            }
            if (categoryItem.GUID == Config.LocalData.Object_Offizielle_Antwort.GUID)
            {
                return 3;
            }

            return -1;
        }
    }
}
