﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Converters
{
    public static class CategoryToBadgeClassConverter
    {
        public static string ConvertCategoryToBadgeClass(clsOntologyItem categoryItem)
        {
            if (categoryItem == null)
            {
                return "-";
            }
            if (categoryItem.GUID == Config.LocalData.Object_Wei__ich_nicht.GUID)
            {
                return Config.LocalData.Object_badge_warning.Name;
            }
            if (categoryItem.GUID == Config.LocalData.Object_Wei__ich.GUID)
            {
                return Config.LocalData.Object_badge_info.Name;
            }
            if (categoryItem.GUID == Config.LocalData.Object_Sitzt.GUID)
            {
                return Config.LocalData.Object_badge_success.Name;
            }
            if (categoryItem.GUID == Config.LocalData.Object_Offizielle_Antwort.GUID)
            {
                return Config.LocalData.Object_badge_secondary.Name;
            }

            return "-";
        }

        public static string ConvertCategoryToBadgeClassPrefix(clsOntologyItem categoryItem)
        {
            if (categoryItem == null || categoryItem.GUID == Config.LocalData.Object_Wei__ich_nicht.GUID)
            {
                return "not-known";
            }
            if (categoryItem.GUID == Config.LocalData.Object_Wei__ich.GUID)
            {
                return "known";
            }
            if (categoryItem.GUID == Config.LocalData.Object_Sitzt.GUID)
            {
                return "internal";
            }
            if (categoryItem.GUID == Config.LocalData.Object_Offizielle_Antwort.GUID)
            {
                return "official";
            }

            return "not-done";
        }
    }
}
