﻿using HtmlEditorModule;
using LearningModule.Factories;
using LearningModule.Models;
using LearningModule.Services;
using LearningModule.Validation;
using MediaViewerModule.Connectors;
using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;

namespace LearningModule
{
    public class LearningController : AppController
    {
        public clsObjectAtt CountBeforeSitztAttribute { get; private set; }

        public clsOntologyItem UnknownCategory { get; private set; }

        public List<clsObjectRel> BaseConfigToCategories { get; private set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CategoriesToShortcuts { get; private set; } = new List<clsObjectRel>();

        public async Task<ResultItem<GetRelatedQuestionsResult>> GetRelatedQuestions(GetRelatedQuestionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetRelatedQuestionsResult>>(async() =>
           {
               var result = new ResultItem<GetRelatedQuestionsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetRelatedQuestionsResult()
               };

               var serviceAgent = new ElasticAgent(Globals);

               if (request.ReferenceItem != null && !string.IsNullOrEmpty(request.ReferenceItem.GUID) && string.IsNullOrEmpty( request.ReferenceItem.Name))
               {
                   request.MessageOutput?.OutputInfo("Get full ReferenceItem...");
                   var getOItemResult = await serviceAgent.GetOItem(request.ReferenceItem.GUID, Globals.Type_Object);
                   result.ResultState = getOItemResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the RefernceItem!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   request.ReferenceItem = getOItemResult.Result;
                   request.MessageOutput?.OutputInfo("Have full ReferenceItem.");
               }

               request.MessageOutput?.OutputInfo("Validate request...");

               result.ResultState = ValidationController.ValidateGetRelatedQuestions(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var referenceItems = new List<clsOntologyItem>();

               if (request.ReferenceItem.GUID_Parent != Config.LocalData.Class_Filter_Config__Learninig_Module_.GUID)
               {
                   referenceItems.Add(request.ReferenceItem);
               }
               else
               {
                   var getFilterRequest = new GetFilterRequest(request.ReferenceItem.GUID) { MessageOutput = request.MessageOutput };
                   var getFilterResult = await GetFilter(getFilterRequest);
                   result.ResultState = getFilterResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }
                   referenceItems.AddRange(getFilterResult.Result.FilterItems);
                   result.Result.Categories = getFilterResult.Result.FilterCategories;
                   result.Result.IncludeLowerCategories = getFilterResult.Result.IncludeLowerCategories;
               }
               

               request.MessageOutput?.OutputInfo("Validated request.");

               var referenceItemsMediaItems = new List<clsOntologyItem>();
               var referenceItemsTypedTags = new List<clsOntologyItem>();

               foreach (var referenceItem in referenceItems)
               {
                   if (referenceItem.GUID_Parent == Config.LocalData.Class_Media_Item.GUID)
                   {
                       referenceItemsMediaItems.Add(referenceItem);
                   }
                   else
                   {
                       referenceItemsTypedTags.Add(referenceItem);
                   }
               }

               if (referenceItemsTypedTags.Any())
               {
                   request.MessageOutput?.OutputInfo("Get TypedTags of ReferenceItem....");
                   var typedTaggingController = new TypedTaggingConnector(Globals);
                   var getTagsResult = await typedTaggingController.GetTags(referenceItemsTypedTags);

                   result.ResultState = getTagsResult.Result;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Questions of ReferenceItem!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   request.MessageOutput?.OutputInfo("Have TypedTags of ReferenceItem.");

                   request.MessageOutput?.OutputInfo("Get Questions...");
                   var questionTags = getTagsResult.TypedTags.OrderBy(typedTag => typedTag.OrderId).Where(typedTag => typedTag.IdTagParent == Config.LocalData.Class_Fragen.GUID).ToList();

                   if (questionTags.Count == 0)
                   {
                       request.MessageOutput?.OutputInfo("No Questions found.");
                       return result;
                   }

                   result.Result.Questions.AddRange(questionTags.Select(tag => new Question
                   {
                       //TypedTag = tag,
                       ReferencedItem = new clsOntologyItem
                       {
                           GUID = tag.IdTagSource,
                           Name = tag.NameTagSource,
                           GUID_Parent = tag.IdParentTagSource,
                           Type = Globals.Type_Object
                       },
                       Reference = new clsOntologyItem
                       {
                           GUID = tag.IdTag,
                           Name = tag.NameTag,
                           GUID_Parent = tag.IdTagParent,
                           Type = tag.TagType
                       }
                   }));
               }

               if (referenceItemsMediaItems.Any())
               {
                   var mediaTaggingController = new MediaTaggingController(Globals);
                   var getMediaBookmarksRequest = new GetMediaBookmarkModelRequest(referenceItemsMediaItems.Select(mediaItem => mediaItem.GUID).ToList(), MediaBookmarkModelRequestType.ByMediaItemIds)
                   {
                       MessageOutput = request.MessageOutput
                   };

                   var bookmarksResult = await mediaTaggingController.GetMediaBookmarks(getMediaBookmarksRequest);
                   result.ResultState = bookmarksResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   
                   foreach (var bookmark in bookmarksResult.Result.OrderBy(bookM => bookM.Second))
                   {
                       var questionReferences = bookmark.References.Where(refItm => refItm.GUID_Parent == Config.LocalData.Class_Fragen.GUID).ToList();
                       if (questionReferences.Any())
                       {
                           
                           result.Result.Questions.AddRange(questionReferences.Select(refItem =>  new Question
                           {
                               ReferencedItem = new clsOntologyItem
                               {
                                   GUID = bookmark.IdMediaItem,
                                   Name = bookmark.NameMediaItem,
                                   GUID_Parent = Config.LocalData.Class_Media_Item.GUID,
                                   Type = Globals.Type_Object
                               },
                               Reference = new clsOntologyItem
                               {
                                   GUID = refItem.GUID,
                                   Name = refItem.Name,
                                   GUID_Parent = refItem.GUID_Parent,
                                   Type = refItem.Type
                               }
                           }));
                       }
                   }
               }

               if (referenceItemsTypedTags.Count > referenceItemsMediaItems.Count)
               {
                   result.Result.ReferenceSource = QuestionReferenceSource.TypedTagging;
               }
               else
               {
                   result.Result.ReferenceSource = QuestionReferenceSource.MediaItem;
               }

               request.MessageOutput?.OutputInfo($"Have {result.Result.Questions.Count} Questions.");

               request.MessageOutput?.OutputInfo("Enrich Questions with details and answers...");

               var enrichResult1 = await serviceAgent.EnrichQuestions(result.Result.Questions);
               result.ResultState = enrichResult1.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Enriched Questions.");

               request.MessageOutput?.OutputInfo("Enrich Answers with details and categories...");

               var answers = result.Result.Questions.SelectMany(question => question.PossibleAnswers).ToList();
               answers.AddRange(result.Result.Questions.SelectMany(question => question.GetGivenAnswers()));
               var enrichResult2 = await serviceAgent.EnrichAnswers(answers);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               result.Result.Questions.ForEach(quest => quest.SetCategory(UnknownCategory, BaseConfigToCategories, CategoriesToShortcuts));

               request.MessageOutput?.OutputInfo("Enrich Answers with details and categories.");

               

               var htmlEditorController = new HtmlEditorConnector(Globals);

               request.MessageOutput?.OutputInfo("Get Html of Answers...");
               var references = result.Result.Questions.Select(quest => quest.QuestionItem.GUID).ToList();
               references.AddRange(result.Result.Questions.SelectMany(quest => quest.PossibleAnswers.Select(answ => answ.AnswerItem.GUID)));
               references.AddRange(result.Result.Questions.SelectMany(quest => quest.GetGivenAnswers().Select(answ => answ.AnswerItem.GUID)));

               var htmlOfQuestionsResult = await htmlEditorController.GetHtmlDocuments(references);
               result.ResultState = htmlOfQuestionsResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               foreach (var question in result.Result.Questions)
               {
                   question.Html = htmlOfQuestionsResult.Result.FirstOrDefault(doc => doc.OItemRef.GUID == question.QuestionItem.GUID)?.OAItemHtmlDocument;
                   var answersOfQuestion = question.PossibleAnswers.Select(answ => answ).ToList();
                   answersOfQuestion.AddRange(question.GetGivenAnswers());
                   foreach (var answer in answersOfQuestion)
                   {
                       answer.Html = htmlOfQuestionsResult.Result.FirstOrDefault(doc => doc.OItemRef.GUID == answer.AnswerItem.GUID)?.OAItemHtmlDocument;
                   }
               }
               request.MessageOutput?.OutputInfo("Have Html of Answers.");

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<GetFilterResult>> GetFilter(GetFilterRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetFilterResult>>(async() =>
           {
               var result = new ResultItem<GetFilterResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetFilterResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               var validationResult = ValidationController.ValidateGetFilterRequest(request, Globals);

               result.ResultState = validationResult;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");


               request.MessageOutput?.OutputInfo("Get Filter-Config...");
               var elasticAgent = new ElasticAgent(Globals);

               var getConfigItem = await elasticAgent.GetOItem(request.IdConfig, Globals.Type_Object);

               result.ResultState = getConfigItem.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the filter-configuration!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var filterConfig = getConfigItem.Result;
               if (filterConfig == null)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Filter-Config not found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (filterConfig.GUID_Parent != Config.LocalData.Class_Filter_Config__Learninig_Module_.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Provided Item is no Filter-Configuration!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have Filter-Config.");

               request.MessageOutput?.OutputInfo("Get Filter-Model...");
               var getModelResult = await elasticAgent.GetFitlerModel(filterConfig);

               result.ResultState = getModelResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Have Filter-Model.");

               result.Result.FilterItems = getModelResult.Result.RelatedObjects;
               if (!result.Result.FilterItems.Any())
               {

                   request.MessageOutput?.OutputInfo("Get ontology...");
                   var ontologyConnector = new OntologyConnector(Globals);

                   var getOntologiesRequest = new OntologyItemsModule.Services.GetOntologyRequest(filterConfig);
                   var ontologyResult = await ontologyConnector.GetOntologies(getOntologiesRequest);
                   result.ResultState = ontologyResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   var enrichResult = await ontologyConnector.EnrichOntologies(ontologyResult.Result);
                   result.ResultState = enrichResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   result.Result.FilterItems.AddRange(enrichResult.Result.SelectMany(res => res.Objects));

               }

               result.Result.FilterCategories = getModelResult.Result.Categories;
               result.Result.IncludeLowerCategories = getModelResult.Result.IncludeLowerCategories?.Val_Bit.Value ?? false;
               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<SaveSelfAnswerResult>> SaveSelfAnswer(SaveSelfAnswerRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SaveSelfAnswerResult>>(async () =>
            {
                var result = new ResultItem<SaveSelfAnswerResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SaveSelfAnswerResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateSaveSelfAnswerRequest(request, Globals);
                
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var relationConfig = new clsRelationConfig(Globals);

                var answer = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = request.answerHtml.Length > 255 ? request.answerHtml.Substring(0, 255) : request.answerHtml,
                    GUID_Parent = Config.LocalData.Class_Antworten.GUID,
                    Type = Globals.Type_Object
                };

                var serviceAgent = new ElasticAgent(Globals);

                request.MessageOutput?.OutputInfo("Save answer...");
                result.ResultState = await serviceAgent.SaveObjects(new List<clsOntologyItem> { answer });

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the Answer!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Saved answer.");

                var saveAttributes = new List<clsObjectAtt>();
                saveAttributes.Add(relationConfig.Rel_ObjectAttribute(answer, Config.LocalData.AttributeType_Datetimestamp__Create_, DateTime.Now));

                var saveRelations = new List<clsObjectRel>();
                result.Result.AnswerToCategory = relationConfig.Rel_ObjectRelation(answer, request.AnswerCategory, Config.LocalData.RelationType_is_of_Type);
                saveRelations.Add(result.Result.AnswerToCategory);
                result.Result.QuestionToAnswer = relationConfig.Rel_ObjectRelation(request.QuestionItem, answer, Config.LocalData.RelationType_belonging,true);
                saveRelations.Add(result.Result.QuestionToAnswer);

                request.MessageOutput?.OutputInfo("Save attributes...");
                result.ResultState = await serviceAgent.SaveAttributes(saveAttributes);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the Create-Stamp Attribute!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Save relations...");
                result.ResultState = await serviceAgent.SaveRelations(saveRelations);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the relation between answer and category or the relation between question and answer!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Saved relations.");
                result.Result.AnswerToCategory.Name_Other = request.AnswerCategory.Name;

                var htmlEditorController = new HtmlEditorConnector(Globals);

                request.MessageOutput?.OutputInfo("Save Html-Document...");
                var saveResult = await htmlEditorController.SaveHtmlContent(request.answerHtml, null, answer);

                result.ResultState = saveResult.Result;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the Html-Document!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Saved Html-Document.");

                result.Result.AnswerItem = answer;
                result.Result.HtmlDocument = saveResult.HtmlDocument;
                result.Result.HtmlAttribute = saveResult.HtmlOAttribute;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveAnswerWithOfficialAnswer(SaveAnswerWithOfficialAnswerRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = Globals.LState_Success.Clone();

                request.MessageOutput?.OutputInfo("Validate reference...");

                result = ValidationController.ValidateSaveAnswerWithOfficialAnswerRequest(request, Globals);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated reference.");

                var answers = request.Question.GetGivenAnswers();

                var officialAnswers = answers.Where(answ => answ.AnswerToCategory != null && answ.AnswerToCategory.ID_Other == Config.LocalData.Object_Offizielle_Antwort.GUID).ToList();

                var selfAnswers = answers.Where(answ => answ.AnswerToCategory != null && answ.AnswerToCategory.ID_Other != Config.LocalData.Object_Offizielle_Antwort.GUID).ToList();

                if (!officialAnswers.Any())
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No official Answer found!";
                    return result;
                }

                var answersWeissIchCount = selfAnswers.Count(answ => answ.AnswerToCategory != null && answ.AnswerToCategory.ID_Other == Config.LocalData.Object_OItem_Wei__ich.GUID);
                var lastAnswer = selfAnswers.LastOrDefault();
                clsOntologyItem successCategory = null;
                if (lastAnswer != null && lastAnswer.AnswerToCategory != null && (lastAnswer.AnswerToCategory.ID_Other == Config.LocalData.Object_OItem_Wei__ich.GUID ||
                    lastAnswer.AnswerToCategory.ID_Other == Config.LocalData.Object_Sitzt.GUID) && answersWeissIchCount == 3)
                {
                    successCategory = Config.LocalData.Object_Sitzt;
                }
                else
                {
                    successCategory = Config.LocalData.Object_Wei__ich;
                }
                
                var answersOk = (from idAnswer in request.IdsAnswers
                                 join answerItem in officialAnswers on idAnswer equals answerItem.AnswerItem.GUID
                                 select idAnswer);

                clsOntologyItem category = null;
                if (request.IdsAnswers.Count == answersOk.Count())
                {
                    category = successCategory;
                }
                else
                {
                    category = Config.LocalData.Object_Wei__ich_nicht;
                }

                var answersGiven = (from possibleAnswer in request.Question.PossibleAnswers
                                    join idAnswer in request.IdsAnswers on possibleAnswer.AnswerItem.GUID equals idAnswer
                                    select possibleAnswer).ToList();

                var newAnswer = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = "Answer",
                    GUID_Parent = Config.LocalData.Class_Antworten.GUID,
                    Type = Globals.Type_Object
                };

                var relationConfig = new clsRelationConfig(Globals);
                var attributes = new List<clsObjectAtt>
                {
                    relationConfig.Rel_ObjectAttribute(newAnswer, Config.LocalData.AttributeType_Datetimestamp__Create_, DateTime.Now)
                };

                var relations = answersGiven.Select(answ => relationConfig.Rel_ObjectRelation(newAnswer, answ.AnswerItem, Config.LocalData.RelationType_contains)).ToList();

                if (!relations.Any())
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No given answers provided!";
                    return result;
                }

                var categoryRelation = relationConfig.Rel_ObjectRelation(newAnswer, category, Config.LocalData.RelationType_is_of_Type);
                relations.Add(categoryRelation);

                var questionToAnswer = relationConfig.Rel_ObjectRelation( request.Question.QuestionItem, newAnswer, Config.LocalData.RelationType_belonging, orderId: request.Question.GetGivenAnswers().Count + 1);
                relations.Add(questionToAnswer);

                var elasticAgent = new ElasticAgent(Globals);
                var saveAnswerResult = await elasticAgent.SaveObjects(new List<clsOntologyItem> { newAnswer });

                result = saveAnswerResult;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Save of new answer failed!";
                    return result;
                }

                var saveAttributesResult = await elasticAgent.SaveAttributes(attributes);

                result = saveAttributesResult;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Create-Stamp!";
                    return result;
                }

                var saveRelationsResult = await elasticAgent.SaveRelations(relations);

                result = saveRelationsResult;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Relations!";
                    return result;
                }

                var answer = new Answer
                {
                    AnswerItem = newAnswer,
                    AnswerToCategory = categoryRelation
                };

                request.Question.AddGivenAsnwer(answer);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<QuestionViewItem>> CreateQuestionViewItem(Question question, Answer answer, List<clsOntologyItem> categories, bool includeLowerCategories)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<QuestionViewItem>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new QuestionViewItem()
                };

                question.AddGivenAsnwer(answer);
                question.SetCategory();
                var createResult = await QuestionAnswerViewItemsFactory.CreateQuestionViewItems(new List<Question> { question }, categories, includeLowerCategories, Globals);
                result.ResultState = createResult.ResultState;
                result.Result = createResult.Result.First();

                return result;
            });

            return taskResult;
        }

        private void SetBaseConfig()
        {
            var elasticAgent = new ElasticAgent(Globals);
            var getBaseConfigResult = elasticAgent.GetBaseConfig();

            if (getBaseConfigResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                throw new Exception(getBaseConfigResult.ResultState.Additional1);
            }

            CountBeforeSitztAttribute = getBaseConfigResult.Result.CountBeforeSitzt;
            BaseConfigToCategories = getBaseConfigResult.Result.BaseConfigToCategories;
            CategoriesToShortcuts = getBaseConfigResult.Result.CategoriesToShortcuts;
            UnknownCategory = getBaseConfigResult.Result.CategoryUnknown;
        }

        public LearningController(Globals globals) : base(globals)
        {
            Initialize();
        }

        private void Initialize()
        {
            SetBaseConfig();
        }
    }
}
