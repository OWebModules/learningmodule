﻿using LearningModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<List<Question>>> EnrichQuestions(List<Question> questions)
        {
            var taskResult = await Task.Run<ResultItem<List<Question>>>(() =>
           {
               var result = new ResultItem<List<Question>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = questions
               };

               var searchDetails = questions.Select(question => new clsObjectAtt
               {
                   ID_Object = question.Reference.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Detail.GUID
               }).ToList();

               var dbReaderDetails = new OntologyModDBConnector(globals);

               if (searchDetails.Any())
               {
                   result.ResultState = dbReaderDetails.GetDataObjectAtt(searchDetails);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Details of Questions!";
                       return result;
                   }

                   foreach (var questionDetail in (from question in questions
                                                   join detail in dbReaderDetails.ObjAtts on question.Reference.GUID equals detail.ID_Object
                                                   select new { question, detail }))
                   {
                       questionDetail.question.Detail = questionDetail.detail;
                   }
               }

               var searchAnswers = questions.Select(question => new clsObjectRel
               {
                   ID_Object = question.Reference.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Fragen_belonging_Antworten.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Fragen_belonging_Antworten.ID_Class_Right
               }).ToList();

               searchAnswers.AddRange(questions.Select(question => new clsObjectRel
               {
                   ID_Object = question.Reference.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Fragen_possible_Antworten.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Fragen_possible_Antworten.ID_Class_Right
               }));

               var dbReaderAnswers = new OntologyModDBConnector(globals);

               if (searchAnswers.Any())
               {
                   result.ResultState = dbReaderAnswers.GetDataObjectRel(searchAnswers);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Answers of Questions!";
                       return result;
                   }

                   foreach (var question in questions)
                   {
                       var answerRels = dbReaderAnswers.ObjectRels.OrderBy(answ => answ.OrderID).ThenBy(answ => answ.Name_Other).Where(rel => rel.ID_Object == question.Reference.GUID);
                       question.SetGivenAnswers(answerRels.Where(rel => rel.ID_RelationType == Config.LocalData.ClassRel_Fragen_belonging_Antworten.ID_RelationType).OrderBy(answ => answ.OrderID).Select(rel => new Answer
                       {
                           AnswerItem = new clsOntologyItem
                           {
                               GUID = rel.ID_Other,
                               Name = rel.Name_Other,
                               GUID_Parent = rel.ID_Parent_Other,
                               Type = rel.Ontology
                           }
                       }));

                       question.PossibleAnswers = answerRels.Where(rel => rel.ID_RelationType == Config.LocalData.ClassRel_Fragen_possible_Antworten.ID_RelationType).Select(rel => new Answer
                       {
                           AnswerItem = new clsOntologyItem
                           {
                               GUID = rel.ID_Other,
                               Name = rel.Name_Other,
                               GUID_Parent = rel.ID_Parent_Other,
                               Type = rel.Ontology
                           }
                       }).ToList();
                   }
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<Answer>>> EnrichAnswers(List<Answer> answers)
        {
            var taskResult = await Task.Run<ResultItem<List<Answer>>>(() =>
           {
               var result = new ResultItem<List<Answer>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<Answer>()
               };

               var searchCreateStamps = answers.Select(answer => new clsObjectAtt
               {
                   ID_Object = answer.AnswerItem.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID
               }).ToList();

               var dbReaderCreateStamps = new OntologyModDBConnector(globals);

               if (searchCreateStamps.Any())
               {
                   result.ResultState = dbReaderCreateStamps.GetDataObjectAtt(searchCreateStamps);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Create-Stamps of Answers!";
                       return result;
                   }

                   foreach (var answerDetail in (from answer in answers
                                                 join stamp in dbReaderCreateStamps.ObjAtts on answer.AnswerItem.GUID equals stamp.ID_Object
                                                 select new { answer, stamp }))
                   {
                       answerDetail.answer.CreateStamp = answerDetail.stamp;
                   }
               }

               var searchDescription = answers.Select(answer => new clsObjectAtt
               {
                   ID_Object = answer.AnswerItem.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Description.GUID
               }).ToList();

               var dbReaderDescription = new OntologyModDBConnector(globals);

               if (searchDescription.Any())
               {
                   result.ResultState = dbReaderDescription.GetDataObjectAtt(searchDescription);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Descriptions of Answers!";
                       return result;
                   }

                   foreach (var answerDetail in (from answer in answers
                                                 join desc in dbReaderDescription.ObjAtts on answer.AnswerItem.GUID equals desc.ID_Object
                                                 select new { answer, desc }))
                   {
                       answerDetail.answer.Detail = answerDetail.desc;
                   }
               }

               var searchCategories = answers.Select(answer => new clsObjectRel
               {
                   ID_Object = answer.AnswerItem.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Antworten_is_of_Type_Wissenskategorie.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Antworten_is_of_Type_Wissenskategorie.ID_Class_Right
               }).ToList();

               var dbReaderCategories = new OntologyModDBConnector(globals);

               if (searchCategories.Any())
               {
                   result.ResultState = dbReaderCategories.GetDataObjectRel(searchCategories);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the categories of answers!";
                       return result;
                   }
                   foreach (var answerCategory in (from answer in answers
                                                   join category in dbReaderCategories.ObjectRels on answer.AnswerItem.GUID equals category.ID_Object
                                                   select new { answer, category }))
                   {
                       answerCategory.answer.AnswerToCategory = answerCategory.category;
                   }
               }
               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<GetFilterModel>> GetFitlerModel(clsOntologyItem filterConfiguration)
        {
            var taskResult = await Task.Run<ResultItem<GetFilterModel>>(() =>
           {
               var result = new ResultItem<GetFilterModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetFilterModel
                   {
                       Config = filterConfiguration
                   }
               };

               var searchFilterConfig = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_AttributeType = Config.LocalData.AttributeType_Include_Lower_Categories.GUID
                   }
               };

               var dbReaderFilterConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderFilterConfig.GetDataObjectAtt(searchFilterConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the include lower categories attribute";
                   return result;
               }

               result.Result.IncludeLowerCategories = dbReaderFilterConfig.ObjAtts.FirstOrDefault();

               var searchCategories = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Filter_Config__Learninig_Module__uses_Wissenskategorie.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Filter_Config__Learninig_Module__uses_Wissenskategorie.ID_Class_Right
                   }
               };

               var dbReaderSearchCategories = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSearchCategories.GetDataObjectRel(searchCategories);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Categories!";
                   return result;
               }

               result.Result.Categories = dbReaderSearchCategories.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (result.Result.IncludeLowerCategories != null && result.Result.IncludeLowerCategories.Val_Bit.Value)
               {
                   if (result.Result.Categories.Any(cat => cat.GUID == Config.LocalData.Object_Sitzt.GUID))
                   {
                       if (!result.Result.Categories.Any(cat => cat.GUID == Config.LocalData.Object_OItem_Wei__ich.GUID))
                       {
                           result.Result.Categories.Add(Config.LocalData.Object_OItem_Wei__ich);
                       }
                   }
                   if (result.Result.Categories.Any(cat => cat.GUID == Config.LocalData.Object_OItem_Wei__ich.GUID))
                   {
                       if (!result.Result.Categories.Any(cat => cat.GUID == Config.LocalData.Object_OItem_Wei__ich_nicht.GUID))
                       {
                           result.Result.Categories.Add(Config.LocalData.Object_OItem_Wei__ich_nicht);
                       }
                   }
               }

               var searchObjects = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object= result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Filter_Config__Learninig_Module__belonging_Object.ID_RelationType
                   }
               };

               var dbReaderObjects = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderObjects.GetDataObjectRel(searchObjects);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the related objects";
               }

               result.Result.RelatedObjects = dbReaderObjects.ObjectRels.Where(rel => rel.Ontology == globals.Type_Object).Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent =rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               return result;
           });

            return taskResult;
        }

        public ResultItem<GetBaseConfigResult> GetBaseConfig()
        {
            var result = new ResultItem<GetBaseConfigResult>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new GetBaseConfigResult()
            };

            var searchBaseConfigToCountBeforeSitz = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_Object = Config.LocalData.Object_Base_Config.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Count_of__Wei__ich__before__Sitzt_.GUID
                }
            };

            var dbReaderCountBeforeSitzt = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderCountBeforeSitzt.GetDataObjectAtt(searchBaseConfigToCountBeforeSitz);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the Attribute CountBeforeSitzt!";
                return result;
            }

            result.Result.CountBeforeSitzt = dbReaderCountBeforeSitzt.ObjAtts.FirstOrDefault();

            result.ResultState = Validation.ValidationController.ValidateGetBaseConfigResult(result.Result, globals, nameof(GetBaseConfigResult.CountBeforeSitzt));

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var searchBaseConfigToCategoryUnknown = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = Config.LocalData.Object_Base_Config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Learning_Module_undefined_Wissenskategorie.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Learning_Module_undefined_Wissenskategorie.ID_Class_Right
                }
            };

            var dbReaderBaseConfigToCategoryUnknown = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderBaseConfigToCategoryUnknown.GetDataObjectRel(searchBaseConfigToCategoryUnknown);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the Category for unknown!";
                return result;
            }

            result.Result.CategoryUnknown = dbReaderBaseConfigToCategoryUnknown.ObjectRels.Select(rel => new clsOntologyItem
            {
                GUID = rel.ID_Other,
                Name = rel.Name_Other,
                GUID_Parent = rel.ID_Parent_Other,
                Type = rel.Ontology
            }).FirstOrDefault();

            result.ResultState = Validation.ValidationController.ValidateGetBaseConfigResult(result.Result, globals, nameof(GetBaseConfigResult.CategoryUnknown));

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var searchCategories = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = Config.LocalData.Object_Base_Config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Learning_Module_contains_Wissenskategorie.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Learning_Module_contains_Wissenskategorie.ID_Class_Right
                }
            };

            var dbReaderCategories = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderCategories.GetDataObjectRel(searchCategories);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the relation between the BaseConfig and Categories!";
                return result;
            }

            result.Result.BaseConfigToCategories = dbReaderCategories.ObjectRels;

            result.ResultState = Validation.ValidationController.ValidateGetBaseConfigResult(result.Result, globals, nameof(GetBaseConfigResult.BaseConfigToCategories));

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var searchCategoriesToShortcuts = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = Config.LocalData.ClassRel_Wissenskategorie_is_defined_by_Shortcut.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_Wissenskategorie_is_defined_by_Shortcut.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Wissenskategorie_is_defined_by_Shortcut.ID_Class_Right
                }
            };

            var dbReaderCategoriesToShortcuts = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderCategoriesToShortcuts.GetDataObjectRel(searchCategoriesToShortcuts);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the relation between Categories and Shortcuts!";
                return result;
            }

            result.Result.CategoriesToShortcuts = dbReaderCategoriesToShortcuts.ObjectRels;

            result.ResultState = Validation.ValidationController.ValidateGetBaseConfigResult(result.Result, globals, nameof(GetBaseConfigResult.CategoriesToShortcuts));

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            return result;
        }

        public ElasticAgent(Globals globals) : base(globals)
        {
        }
    }
}
