﻿using LearningModule.Converters;
using LearningModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LearningModule.Factories
{
    public static class QuestionAnswerViewItemsFactory
    {
        public static async Task<ResultItem<AnswerViewItemResult>> CreatePossibleAnswerViewItems(QuestionViewItem questionItem, List<Question> questions, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<AnswerViewItemResult>>(() =>
            {
                var result = new ResultItem<AnswerViewItemResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new AnswerViewItemResult()
                };

                var question = questions.FirstOrDefault(quest => quest.QuestionItem.GUID == questionItem.IdQuestion);

                if (question == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Question cannot be found!";
                    return result;
                }
                result.Result.MarginLeft = questionItem.WidthNumber;
                result.Result.IdQuestion = question.QuestionItem.GUID;
                result.Result.MultipleAnswers = questionItem.MultipleAnswers;
                result.Result.AnswerList = question.PossibleAnswers.Select(answ => new AnswerViewItem
                {
                    IdAnswer = answ.AnswerItem.GUID,
                    AnswerHtml = answ.ToString(),
                    IdQuestion = question.QuestionItem.GUID
                }).ToList();

                return result;
            });

            return taskResult;
        }

        public static async Task<ResultItem<AnswerViewItemResult>> CreateCorrectAnswerViewItems(QuestionViewItem questionItem, List<Question> questions, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<AnswerViewItemResult>>(() =>
            {
                var result = new ResultItem<AnswerViewItemResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new AnswerViewItemResult()
                };

                var question = questions.FirstOrDefault(quest => quest.QuestionItem.GUID == questionItem.IdQuestion);

                if (question == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Question cannot be found!";
                    return result;
                }
                result.Result.MarginLeft = questionItem.WidthNumber;
                result.Result.IdQuestion = question.QuestionItem.GUID;
                result.Result.MultipleAnswers = questionItem.MultipleAnswers;
                result.Result.AnswerList = question.GetGivenAnswers().Where(answ => answ.AnswerToCategory != null && answ.AnswerToCategory.ID_Other == Config.LocalData.Object_Offizielle_Antwort.GUID).Select(answ => new AnswerViewItem
                {
                    IdAnswer = answ.AnswerItem.GUID,
                    AnswerHtml = answ.ToString(),
                    IdQuestion = question.QuestionItem.GUID
                }).ToList();

                if (!result.Result.AnswerList.Any())
                {
                    result.Result.AnswerList.Add(question.GetGivenAnswers().Where(answ => answ.AnswerToCategory != null && (answ.AnswerToCategory.ID_Other == Config.LocalData.Object_Sitzt.GUID || answ.AnswerToCategory.ID_Other == Config.LocalData.Object_Wei__ich.GUID)).Select(answ => new AnswerViewItem
                    {
                        IdAnswer = answ.AnswerItem.GUID,
                        AnswerHtml = answ.ToString(),
                        IdQuestion = question.QuestionItem.GUID
                    }).FirstOrDefault());
                }

                return result;
            });

            return taskResult;
        }

        public static async Task<ResultItem<List<QuestionViewItem>>> CreateQuestionViewItems(List<Question> questionItems, List<clsOntologyItem> categories, bool includeLowerCategories, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<QuestionViewItem>>>(() =>
              {
                  var result = new ResultItem<List<QuestionViewItem>>
                  {
                      ResultState = globals.LState_Success.Clone(),
                      Result = new List<QuestionViewItem>()
                  };

                  var count = questionItems.Count;
                  var width = 10;
                  if (count >= 10 && count < 100)
                  {
                      width = 20;
                  }
                  else if (count >= 100 && count < 1000 )
                  {
                      width = 30;
                  }
                  else if (count >=1000)
                  {
                      width = 40;
                  }

                  var id = 1;

                  if (!categories.Any())
                  {
                      result.Result = questionItems.Select(quest => new QuestionViewItem
                      {
                          Id = id++,
                          IdQuestion = quest.QuestionItem.GUID,
                          IdReference = quest.ReferencedItem.GUID,
                          NameReference = HttpUtility.HtmlEncode(quest.ReferencedItem.Name),
                          QuestionHtml = quest.ToString(),
                          IsMultipleChoice = quest.PossibleAnswers.Any(),
                          IdCategory = quest.Category?.GUID ?? string.Empty,
                          Category = quest.Category?.Name ?? string.Empty,
                          BadgeClass = CategoryToBadgeClassConverter.ConvertCategoryToBadgeClass(quest.Category),
                          BadgeClassPrefix = CategoryToBadgeClassConverter.ConvertCategoryToBadgeClassPrefix(quest.Category),
                          CategoryCounts = quest.CategoryCounts,
                          CountOfAnswers = quest.CountOfAnswers,
                          NumberCategoriesInSequence = quest.NumberOfLastCagegory,
                          MultipleAnswers = quest.GetGivenAnswers().Where(answ => answ.AnswerToCategory != null && answ.AnswerToCategory.ID_Other == Config.LocalData.Object_Offizielle_Antwort.GUID).Count() > 1,
                          WidthNumber = width
                      }).ToList();
                  }
                  else
                  {
                      result.Result = questionItems.Where(quest => quest.HasCategory(categories, includeLowerCategories)).Select(quest => new QuestionViewItem
                      {
                          Id = id++,
                          IdQuestion = quest.QuestionItem.GUID,
                          IdReference = quest.ReferencedItem.GUID,
                          NameReference = HttpUtility.HtmlEncode(quest.ReferencedItem.Name),
                          QuestionHtml = quest.ToString(),
                          IdCategory = quest.Category?.GUID ?? string.Empty,
                          Category = quest.Category?.Name ?? string.Empty,
                          BadgeClass = CategoryToBadgeClassConverter.ConvertCategoryToBadgeClass(quest.Category),
                          BadgeClassPrefix = CategoryToBadgeClassConverter.ConvertCategoryToBadgeClassPrefix(quest.Category),
                          IsMultipleChoice = quest.PossibleAnswers.Any(),
                          CategoryCounts = quest.CategoryCounts,
                          CountOfAnswers = quest.CountOfAnswers,
                          NumberCategoriesInSequence = quest.NumberOfLastCagegory,
                          MultipleAnswers = quest.GetGivenAnswers().Where(answ => answ.AnswerToCategory != null && answ.AnswerToCategory.ID_Other == Config.LocalData.Object_Offizielle_Antwort.GUID).Count() > 1,
                          WidthNumber = width
                      }).ToList();
                  }
                  

                  return result;
              });

            return taskResult;

        }
    }
}
