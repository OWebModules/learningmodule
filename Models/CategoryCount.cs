﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class CategoryCount
    {
        public string Category { get; set; }
        public string ShortCut { get; set; }
        public int Count { get; set; }

        public string Text { get; set; }
    }
}
