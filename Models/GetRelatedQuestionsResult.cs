﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public enum QuestionReferenceSource
    {
        TypedTagging = 1,
        MediaItem = 2
    }
    public class GetRelatedQuestionsResult
    {
        public QuestionReferenceSource ReferenceSource { get; set; }
        public List<Question> Questions { get; set; } = new List<Question>();
        public List<clsOntologyItem> Categories { get; set; } = new List<clsOntologyItem>();
        public bool IncludeLowerCategories { get; set; }
    }
}
