﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class GetFilterRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }
        public GetFilterRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
