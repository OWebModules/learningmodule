﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class SaveSelfAnswerResult
    {
        public clsOntologyItem AnswerItem { get; set; }
        public clsObjectAtt CreateStamp { get; set; }
        public clsObjectRel AnswerToCategory { get; set; }
        public clsObjectRel QuestionToAnswer { get; set; }

        public clsOntologyItem HtmlDocument { get; set; }

        public clsObjectAtt HtmlAttribute { get; set; }
    }
}
