﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class SaveAnswerWithOfficialAnswerRequest
    {
        public Question Question { get; private set; }
        public List<string> IdsAnswers { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public SaveAnswerWithOfficialAnswerRequest(Question question, List<string> idAnswers)
        {
            Question = question;
            IdsAnswers = idAnswers;
        }
    }
}
