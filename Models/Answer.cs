﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LearningModule.Models
{
    public class Answer
    {
        public clsOntologyItem AnswerItem { get; set; }

        public clsObjectAtt CreateStamp { get; set; }

        public clsObjectAtt Detail { get; set; }

        public clsObjectAtt Html { get; set; }

        public clsObjectRel AnswerToCategory { get; set; }

        public override string ToString()
        {
            var result = "";

            if (Html != null)
            {
                result = Html.Val_String;
            }
            else if (Detail != null)
            {
                result = HttpUtility.HtmlEncode(Detail.Val_String);
            }
            else
            {
                result = HttpUtility.HtmlEncode(AnswerItem.Name);
            }

            return result;
        }
    }
}
