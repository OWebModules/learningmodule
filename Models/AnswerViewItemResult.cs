﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class AnswerViewItemResult
    {
        public int MarginLeft { get; set; }
        public string IdQuestion { get; set; }
        public List<AnswerViewItem> AnswerList { get; set; } = new List<AnswerViewItem>();
        public bool MultipleAnswers { get; set; }
    }
}
