﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class GetBaseConfigResult
    {
        public clsOntologyItem BaseConfig { get; set; }
        public clsObjectAtt CountBeforeSitzt { get; set; }

        public clsOntologyItem CategoryUnknown { get; set; }

        public List<clsObjectRel> BaseConfigToCategories { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> CategoriesToShortcuts { get; set; } = new List<clsObjectRel>();


    }
}
