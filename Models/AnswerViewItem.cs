﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class AnswerViewItem
    {
        public string IdQuestion { get; set; }
        public string IdAnswer { get; set; }
        public string AnswerHtml { get; set; }

    }
}
