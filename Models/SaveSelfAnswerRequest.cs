﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class SaveSelfAnswerRequest
    {
        public clsOntologyItem QuestionItem
        {
            get; set;
        }
        
        public clsOntologyItem AnswerCategory
        {
            get; set;
        }
        
        public string answerHtml
        {
            get; set;
        }

        public IMessageOutput MessageOutput
        {
            get; set;
        }
    }
}
