﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TypedTaggingModule.Models;

namespace LearningModule.Models
{
    public class Question
    {
        public clsOntologyItem ReferencedItem { get; set; }

        private clsOntologyItem reference;
        public clsOntologyItem Reference {
            get { return reference; }
            set
            {
                if (reference == value) return;
                reference = value;
                if (reference == null) return;

                QuestionItem = new clsOntologyItem
                {
                    GUID = reference.GUID,
                    Name = reference.Name,
                    GUID_Parent = reference.GUID_Parent,
                    Type = reference.Type
                };
            }
        }

        public clsOntologyItem QuestionItem { get; private set; }
        public clsObjectAtt Detail { get; set; }

        public clsObjectAtt Html { get; set; }

        public clsOntologyItem Category { get; private set; }

        public clsOntologyItem UnknownCategory { get; private set; }

        public List<clsObjectRel> BaseConfigToCategories { get; private set; } = new List<clsObjectRel>();

        public List<clsObjectRel> CategoriesToShortcuts { get; private set; } = new List<clsObjectRel>();

        public List<Answer> PossibleAnswers { get; set; } = new List<Answer>();
        private List<Answer> givenAnswers = new List<Answer>();

        public int NumberOfLastCagegory { get; private set; }
        public int CountOfAnswers { get; private set; }

        public List<CategoryCount> CategoryCounts { get; set; } = new List<CategoryCount>();
        public void SetStatsOfCategories()
        {
            var answer = givenAnswers.LastOrDefault();
            var categoryId = answer?.AnswerToCategory?.ID_Other;
            CategoryCounts = new List<CategoryCount>();
            NumberOfLastCagegory = 0;
            if (answer == null || categoryId == null) return;

            for (int ix = givenAnswers.Count - 1; ix >= 0; ix--)
            {
                var answer1 = givenAnswers[ix];
                if (answer1.AnswerToCategory != null && answer1.AnswerToCategory.ID_Other == categoryId)
                {
                    NumberOfLastCagegory ++;
                }
                if (answer1.AnswerToCategory == null) break;
                if (answer1.AnswerToCategory.ID_Other != categoryId) break;
            }

            CountOfAnswers = givenAnswers.Where(answ => answ.AnswerToCategory.ID_Other != Config.LocalData.Object_Offizielle_Antwort.GUID).Count();
            if (!givenAnswers.Any())
            {
                CategoryCounts.Add(new CategoryCount { Category = UnknownCategory.Name, Count = givenAnswers.Where(answ => answ.AnswerToCategory == null).Count(), ShortCut = "UC" , Text = "UC"});
            }
            else
            {
                var answersWithCategory = givenAnswers.GroupBy(answ => new { answ.AnswerToCategory.ID_Other, answ.AnswerToCategory.Name_Other }).Select(answ => new { answ.Key.ID_Other, answ.Key.Name_Other, Count = answ.Count() });
                CategoryCounts.AddRange(from answerWithCategory in answersWithCategory
                                        join categoryToShortcut in CategoriesToShortcuts on answerWithCategory.ID_Other equals categoryToShortcut.ID_Object
                                        select new CategoryCount
                                        {
                                            Category = answerWithCategory.Name_Other,
                                            Count = answerWithCategory.Count,
                                            ShortCut = categoryToShortcut.Name_Other,
                                            Text = $"{categoryToShortcut.Name_Other}:{answerWithCategory.Count}"
                                        });
            }
            
        }

        public List<Answer> GetGivenAnswers()
        {
            return givenAnswers;
        }

        public void SetGivenAnswers(IEnumerable<Answer> givenAnswers)
        {
            this.givenAnswers = givenAnswers.ToList();
            
        }

        public void AddGivenAsnwer(Answer givenAnswer)
        {
            this.givenAnswers.Add(givenAnswer);
            SetStatsOfCategories();
        }

        public void SetCategory(clsOntologyItem unknownCategory = null, List<clsObjectRel> baseConfigToCategories = null, List<clsObjectRel> categoriesToShortcuts = null)
        {
            var lastGivenAnswer = givenAnswers.Where(answ => answ.AnswerToCategory != null).LastOrDefault();
            if (lastGivenAnswer != null)
            {
                Category = new clsOntologyItem
                {
                    GUID = lastGivenAnswer.AnswerToCategory.ID_Other,
                    Name = lastGivenAnswer.AnswerToCategory.Name_Other,
                    GUID_Parent = lastGivenAnswer.AnswerToCategory.ID_Parent_Other,
                    Type = lastGivenAnswer.AnswerToCategory.Ontology
                };
            }
            if (unknownCategory != null) UnknownCategory = unknownCategory;
            if (baseConfigToCategories != null)  BaseConfigToCategories = baseConfigToCategories;
            if (categoriesToShortcuts != null) CategoriesToShortcuts = categoriesToShortcuts;
            SetStatsOfCategories();
        }

        public bool HasCategory(List<clsOntologyItem> categories, bool includeLowerCategories)
        {

            if (!categories.Any())
            {
                return true;
            }

            if (Category == null)
            {
                if (categories.Any() && !includeLowerCategories)
                {
                    return false;
                }
                
                if (categories.Any() && includeLowerCategories)
                {
                    return true;
                }
            }

            if (includeLowerCategories && categories.Any(cat => cat.GUID == Config.LocalData.Object_Sitzt.GUID))
            {
                if (!categories.Any(cat => cat.GUID == Config.LocalData.Object_Wei__ich.GUID))
                {
                    categories.Add(Config.LocalData.Object_Wei__ich);
                }
                if (!categories.Any(cat => cat.GUID == Config.LocalData.Object_Wei__ich_nicht.GUID))
                {
                    categories.Add(Config.LocalData.Object_Wei__ich_nicht);
                }
            }

            if (includeLowerCategories && categories.Any(cat => cat.GUID == Config.LocalData.Object_Wei__ich.GUID))
            {
                if (!categories.Any(cat => cat.GUID == Config.LocalData.Object_Wei__ich_nicht.GUID))
                {
                    categories.Add(Config.LocalData.Object_Wei__ich_nicht);
                }
            }

            if (includeLowerCategories)
            {
                if (categories.Any(cat => cat.GUID == Category.GUID) && Category == null)
                {
                    return true;
                }
            }
            else
            {
                if (categories.Any(cat => cat.GUID == Category.GUID))
                {
                    return true;
                }
            }

            return false;
        }

        public override string ToString()
        {
            var result = "";

            if (Html != null)
            {
                result = Html.Val_String;
            }
            else if (Detail != null)
            {
                result = HttpUtility.HtmlEncode(Detail.Val_String);
            }
            else
            {
                result = HttpUtility.HtmlEncode(QuestionItem.Name);
            }

            return result;
        }

    }
}
