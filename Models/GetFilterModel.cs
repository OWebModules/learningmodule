﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class GetFilterModel
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectAtt IncludeLowerCategories { get; set; }
        public List<clsOntologyItem> Categories { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> RelatedObjects { get; set; } = new List<clsOntologyItem>();
    }
}
