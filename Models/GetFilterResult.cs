﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class GetFilterResult
    {
        public List<clsOntologyItem> FilterItems { get; set; }
        public List<clsOntologyItem> FilterCategories { get; set; } = new List<clsOntologyItem>();
        public bool IncludeLowerCategories { get; set; }

    }
}
