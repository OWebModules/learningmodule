﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class QuestionViewItem
    {
        public int Id { get; set; }
        public string IdQuestion { get; set; }
        public string IdReference { get; set; }
        public string NameReference { get; set; }
        public string QuestionHtml { get; set; }
        public bool IsMultipleChoice { get; set; }
        public bool MultipleAnswers { get; set; }
        public int WidthNumber { get; set; }

        public string IdCategory { get; set; }
        public string Category { get; set; }

        public string IdCategroyKnown { get; private set; }
        public string IdCategoryNotKnown { get; private set; }
        public string IdCategoryInternalized { get; private set; }
        public string IdCategoryOfficial { get; private set; }
        public string BadgeClassPrefix { get; set; }
        public string BadgeClass { get; set; }
        public List<CategoryCount> CategoryCounts { get; set; }
        public int CountOfAnswers { get; set; }
        public int NumberCategoriesInSequence { get; set; }

        public QuestionViewItem()
        {
            IdCategoryNotKnown = Config.LocalData.Object_Wei__ich_nicht.GUID;
            IdCategroyKnown = Config.LocalData.Object_Wei__ich.GUID;
            IdCategoryInternalized = Config.LocalData.Object_Sitzt.GUID;
            IdCategoryOfficial = Config.LocalData.Object_Offizielle_Antwort.GUID;
        }
    }
}
