﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Models
{
    public class GetRelatedQuestionsRequest
    {
        public clsOntologyItem ReferenceItem { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetRelatedQuestionsRequest(clsOntologyItem referenceItem)
        {
            ReferenceItem = referenceItem;
        }
    }
}
