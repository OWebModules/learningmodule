﻿using LearningModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateGetRelatedQuestions(GetRelatedQuestionsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (request.ReferenceItem == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No ReferenceItem provided!";
                return result;
            }

            if (request.ReferenceItem.Type != globals.Type_Object)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The ReferenceItem must be an object!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetFilterRequest(GetFilterRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is not a valid Guid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateSaveSelfAnswerRequest(SaveSelfAnswerRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (request == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a request!";
                return result;
            }

            if (request.QuestionItem == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a question!";
                return result;
            }

            if (request.QuestionItem.GUID_Parent != Config.LocalData.Class_Fragen.GUID)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The provided object for the question parameter is no question!";
                return result;

            }

            if (request.AnswerCategory == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a category for the answer!";
                return result;
            }

            if (request.AnswerCategory.GUID != Config.LocalData.Object_Wei__ich_nicht.GUID &&
                request.AnswerCategory.GUID != Config.LocalData.Object_Wei__ich.GUID &&
                request.AnswerCategory.GUID != Config.LocalData.Object_Sitzt.GUID &&
                request.AnswerCategory.GUID != Config.LocalData.Object_Offizielle_Antwort.GUID)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The provided object for the category paramter is no category!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateSaveAnswerWithOfficialAnswerRequest(SaveAnswerWithOfficialAnswerRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (request == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a request!";
                return result;
            }

            if (request.Question == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a question!";
                return result;
            }

            if (!request.IdsAnswers.Any())
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide at least one idAnswer!";
                return result;

            }

            if (request.IdsAnswers.Any(id => string.IsNullOrEmpty(id)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "There is at least one idAnswer which is empty!";
                return result;
            }

            if (!request.IdsAnswers.All(id=> globals.is_GUID(id)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "There is at least one idAnswer which is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetBaseConfigResult(GetBaseConfigResult baseConfig, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetBaseConfigResult.CountBeforeSitzt))
            {

                if (baseConfig.CountBeforeSitzt == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Attribute CountBeforeSitzt found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetBaseConfigResult.CategoryUnknown))
            {

                if (baseConfig.CategoryUnknown == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Category for unknown found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetBaseConfigResult.BaseConfigToCategories))
            {

                if (!baseConfig.BaseConfigToCategories.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Categories assigned";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetBaseConfigResult.CategoriesToShortcuts))
            {

                if (!baseConfig.CategoriesToShortcuts.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Shortcuts for Categories found!";
                    return result;
                }
            }

            return result;
        }
    }
}
